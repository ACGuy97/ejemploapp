package com.barranquero.ejemploapp;

/**
 * Created by usuario on 23/02/17
 * EjemploApp
 */
public class Product {
    public String name;

    public Product() {
    }

    public int category;

    public Product(int category, String name) {
        this.category = category;
        this.name = name;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
