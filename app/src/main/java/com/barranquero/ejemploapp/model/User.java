package com.barranquero.ejemploapp.model;

/**
 * Created by usuario on 23/02/17
 * EjemploApp
 */
public class User {
    public String email;
    public String name;

    public User(String email, String displayName) {
        this.email = email;
        this.name = displayName;
    }
}
